-------------------------------------------------------------------------------
--
-- Title       : comp
-- Design      : laba4
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : comp.vhd
-- Generated   : Mon Apr 24 17:20:59 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {comp} architecture {comp}}

library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use ieee.std_logic_unsigned.all;

entity comp is
	 port(
	 Bout : out STD_LOGIC  ;
	 Ainout : inout STD_logic
	     );
end comp;

--}} End of automatically maintained section

           architecture comp of comp is 
signal CLK:std_logic; 
begin
Pr_CLK: process
begin	
	CLK <= '0' ;
    wait for 10 ns;
	CLK <= '1';	
	wait for 10 ns;
end process Pr_CLK;
Pr_A: process
begin
  wait until CLK ='0';
  Ainout<='0' after 5 ns; 
  wait until CLK = '1';
  Ainout<='1' after 5 ns;
  wait for 20 ns;
 end process Pr_A;
Pr_B: process
begin			
   wait on Ainout;
   Bout <= not Ainout;
end process Pr_B;
end comp;

