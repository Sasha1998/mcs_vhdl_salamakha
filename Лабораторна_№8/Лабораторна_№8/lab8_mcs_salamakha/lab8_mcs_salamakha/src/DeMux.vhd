-------------------------------------------------------------------------------
--
-- Title       : DeMux
-- Design      : lab8_mcs_manko
-- Author      : Danylo Manko
-- Company     : NU LP
--
-------------------------------------------------------------------------------
--
-- File        : DeMux.vhd
-- Generated   : Thu Apr 27 18:59:30 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {DeMux} architecture {DeMux}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity DeMux is
	port(
		CLK : in STD_LOGIC;
		X : in STD_LOGIC_VECTOR(31 downto 0);
		Y7 : out STD_LOGIC_VECTOR(3 downto 0);
		Y6 : out STD_LOGIC_VECTOR(3 downto 0);
		Y5 : out STD_LOGIC_VECTOR(3 downto 0);
		Y4 : out STD_LOGIC_VECTOR(3 downto 0);
		Y3 : out STD_LOGIC_VECTOR(3 downto 0);
		Y2 : out STD_LOGIC_VECTOR(3 downto 0);
		Y1 : out STD_LOGIC_VECTOR(3 downto 0);
		Y0 : out STD_LOGIC_VECTOR(3 downto 0)
		);
end DeMux;


architecture DeMux of DeMux is		
begin  
	process( X,CLK)	
	begin  
		if  CLK = '1'  then
			Y0(0) <= X(0);
			Y0(1) <= X(1);
			Y0(2) <= X(2);
			Y0(3) <= X(3);
			Y1(0) <= X(4);
			Y1(1) <= X(5);
			Y1(2) <= X(6);
			Y1(3) <= X(7);
			Y2(0) <= X(8);
			Y2(1) <= X(9);
			Y2(2) <= X(10);
			Y2(3) <= X(11);
			Y3(0) <= X(12);
			Y3(1) <= X(13);
			Y3(2) <= X(14);
			Y3(3) <= X(15);
			Y4(0) <= X(16);	
			Y4(1) <= X(17);
			Y4(2) <= X(18);
			Y4(3) <= X(19);
			Y5(0) <= X(20);
			Y5(1) <= X(21);
			Y5(2) <= X(22);
			Y5(3) <= X(23);
			Y6(0) <= X(24);
			Y6(1) <= X(25);
			Y6(2) <= X(26);
			Y6(3) <= X(27);
			Y7(0) <= X(28);
			Y7(1) <= X(29);
			Y7(2) <= X(30);	  
			Y7(3) <= X(31);
		else
			Y0 <= "UUUU";	 
			Y1 <= "UUUU";
			Y2 <= "UUUU";
			Y3 <= "UUUU";
			Y4 <= "UUUU";
			Y5 <= "UUUU";
			Y6 <= "UUUU";
			Y7 <= "UUUU";	
		end if;  
	end process;								 					 
end DeMux;