-------------------------------------------------------------------------------
--
-- Title       : DC
-- Design      : lab2_mcs_manko
-- Author      : Danylo Manko
-- Company     : NU LP
--
-------------------------------------------------------------------------------
--
-- File        : DC.vhd
-- Generated   : Mon Mar  6 12:11:25 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {DC} architecture {DC}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;  
use ieee.std_logic_unsigned.all;

entity DC is
	 port(
		 A : in STD_LOGIC_VECTOR(3 downto 0);
		 F : out STD_LOGIC_VECTOR(6 downto 0)
	     );
end DC;

--}} End of automatically maintained section

architecture DC of DC is
begin
	   with A select
	F <= "1110111" when "0000",
			"0100100" when "0001",
			"1011101" when "0010",
			"1101101" when "0011",
			"0101110" when "0100",
			"1101011" when "0101",
			"1111011" when "0110",
			"0100101" when "0111",
			"1111111" when "1000",
			"1101111" when "1001",
			"0000000" when others;

end DC;
