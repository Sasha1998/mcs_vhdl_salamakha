-------------------------------------------------------------------------------
--
-- Title       : P_RG
-- Design      : lab5_mcs_manko
-- Author      : Danylo Manko
-- Company     : NU LP
--
-------------------------------------------------------------------------------
--
-- File        : P_RG.vhd
-- Generated   : Thu Apr 20 22:46:38 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {P_RG} architecture {P_RG}}

library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use ieee.std_logic_unsigned.all;

entity P_RG is
  generic(
       Capacity : INTEGER := 32
  );
  port (
       CLK : in STD_LOGIC;
       DATA_in : in STD_LOGIC_VECTOR(Capacity-1 downto 0);
       WE : in STD_LOGIC;
       DATA_out : out STD_LOGIC_VECTOR(Capacity-1 downto 0)
  );
   end;

--}} End of automatically maintained section

architecture P_RG of P_RG is
begin
		process
	variable DATA : std_logic_vector(Capacity-1 downto 0);
	begin
	wait until CLK = '1';
	if WE = '1'
		then DATA := DATA_IN;
	else 
	   DATA_OUT <= DATA;
	end if;
	end process;
end P_RG;
