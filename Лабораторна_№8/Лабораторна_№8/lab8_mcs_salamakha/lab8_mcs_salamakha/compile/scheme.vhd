-------------------------------------------------------------------------------
--
-- Title       : scheme
-- Design      : lab8_mcs_manko
-- Author      : Danylo Manko
-- Company     : NU LP
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab8_mcs_manko\lab8_mcs_manko\compile\scheme.vhd
-- Generated   : Thu Apr 27 19:06:06 2017
-- From        : c:\My_Designs\lab8_mcs_manko\lab8_mcs_manko\src\scheme.bde
-- By          : Bde2Vhdl ver. 2.6
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------
-- Design unit header --
library IEEE;
use IEEE.std_logic_1164.all;


entity scheme is
  port(
       CLK : in STD_LOGIC;
       WE : in STD_LOGIC;
       X : in STD_LOGIC_VECTOR(31 downto 0);
       LCD0 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD1 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD2 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD3 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD4 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD5 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD6 : out STD_LOGIC_VECTOR(6 downto 0);
       LCD7 : out STD_LOGIC_VECTOR(6 downto 0)
  );
end scheme;

architecture scheme of scheme is

---- Component declarations -----

component DC
  port (
       A : in STD_LOGIC_VECTOR(3 downto 0);
       F : out STD_LOGIC_VECTOR(6 downto 0)
  );
end component;
component DeMux
  port (
       CLK : in STD_LOGIC;
       X : in STD_LOGIC_VECTOR(31 downto 0);
       Y0 : out STD_LOGIC_VECTOR(3 downto 0);
       Y1 : out STD_LOGIC_VECTOR(3 downto 0);
       Y2 : out STD_LOGIC_VECTOR(3 downto 0);
       Y3 : out STD_LOGIC_VECTOR(3 downto 0);
       Y4 : out STD_LOGIC_VECTOR(3 downto 0);
       Y5 : out STD_LOGIC_VECTOR(3 downto 0);
       Y6 : out STD_LOGIC_VECTOR(3 downto 0);
       Y7 : out STD_LOGIC_VECTOR(3 downto 0)
  );
end component;
component P_RG
  generic(
       Capacity : INTEGER := 8
  );
  port (
       CLK : in STD_LOGIC;
       DATA_in : in STD_LOGIC_VECTOR(Capacity-1 downto 0);
       WE : in STD_LOGIC;
       DATA_out : out STD_LOGIC_VECTOR(Capacity-1 downto 0)
  );
end component;

----     Constants     -----
constant DANGLING_INPUT_CONSTANT : STD_LOGIC := 'Z';

---- Signal declarations used on the diagram ----

signal BUS259 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS275 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS284 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS293 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS302 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS311 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS320 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS329 : STD_LOGIC_VECTOR (3 downto 0);
signal BUS374 : STD_LOGIC_VECTOR (7 downto 0);

---- Declaration for Dangling input ----
signal Dangling_Input_Signal : STD_LOGIC;

begin

----  Component instantiations  ----

U1 : P_RG
  port map(
       CLK => CLK,
       DATA_in => X( 31 downto 0 ),
       DATA_out => BUS374( 7 downto 0 ),
       WE => WE
  );

U10 : DC
  port map(
       A => BUS259,
       F => LCD0
  );

U2 : DeMux
  port map(
       CLK => CLK,
       X(0) => Dangling_Input_Signal,
       X(1) => Dangling_Input_Signal,
       X(2) => Dangling_Input_Signal,
       X(3) => Dangling_Input_Signal,
       X(4) => Dangling_Input_Signal,
       X(5) => Dangling_Input_Signal,
       X(6) => Dangling_Input_Signal,
       X(7) => Dangling_Input_Signal,
       X(8) => Dangling_Input_Signal,
       X(9) => Dangling_Input_Signal,
       X(10) => Dangling_Input_Signal,
       X(11) => Dangling_Input_Signal,
       X(12) => Dangling_Input_Signal,
       X(13) => Dangling_Input_Signal,
       X(14) => Dangling_Input_Signal,
       X(15) => Dangling_Input_Signal,
       X(16) => Dangling_Input_Signal,
       X(17) => Dangling_Input_Signal,
       X(18) => Dangling_Input_Signal,
       X(19) => Dangling_Input_Signal,
       X(20) => Dangling_Input_Signal,
       X(21) => Dangling_Input_Signal,
       X(22) => Dangling_Input_Signal,
       X(23) => Dangling_Input_Signal,
       X(24) => BUS374(0),
       X(25) => BUS374(1),
       X(26) => BUS374(2),
       X(27) => BUS374(3),
       X(28) => BUS374(4),
       X(29) => BUS374(5),
       X(30) => BUS374(6),
       X(31) => BUS374(7),
       Y0 => BUS329,
       Y1 => BUS320,
       Y2 => BUS311,
       Y3 => BUS302,
       Y4 => BUS293,
       Y5 => BUS284,
       Y6 => BUS275,
       Y7 => BUS259
  );

U3 : DC
  port map(
       A => BUS293,
       F => LCD3
  );

U4 : DC
  port map(
       A => BUS302,
       F => LCD4
  );

U5 : DC
  port map(
       A => BUS311,
       F => LCD5
  );

U6 : DC
  port map(
       A => BUS320,
       F => LCD6
  );

U7 : DC
  port map(
       A => BUS329,
       F => LCD7
  );

U8 : DC
  port map(
       A => BUS284,
       F => LCD2
  );

U9 : DC
  port map(
       A => BUS275,
       F => LCD1
  );


---- Dangling input signal assignment ----

Dangling_Input_Signal <= DANGLING_INPUT_CONSTANT;

end scheme;
