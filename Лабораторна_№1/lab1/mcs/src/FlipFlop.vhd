-------------------------------------------------------------------------------
--
-- Title       : FlipFlop
-- Design      : mcs
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : FlipFlop.vhd
-- Generated   : Mon Apr 10 10:44:19 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {FlipFlop} architecture {FlipFlop}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity FlipFlop is
	 port(
		 R : in STD_LOGIC;
		 S : in STD_LOGIC;
		 Q : buffer STD_LOGIC;
		 NQ : buffer STD_LOGIC
	     );
end FlipFlop;

--}} End of automatically maintained section

architecture FlipFlop of FlipFlop is
begin

	process (R,S)
begin  
if R='0' and S = '0' then
		Q<=Q;
		NQ<= NOT Q;
	end if;	 
	if R='0' and S = '1' then
		Q<='1';
		NQ<= NOT Q;
	end if;
	if R='1' and S = '0' then
		Q<='0';
		NQ<= NOT Q;
	end if;
	if R='0' and S = '0' then
		Q<= NOT Q;
		NQ<= NOT Q;
		end if;
end process	;

end FlipFlop;
