-------------------------------------------------------------------------------
--
-- Title       : Counter
-- Design      : mcs
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : Counter.vhd
-- Generated   : Mon Apr 10 11:01:08 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Counter} architecture {Counter}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;
use IEEE.STD_LOGIC_UNSIGNED.ALL;

entity Counter is
	 port(
		 CLK : in STD_LOGIC;
		 RST : in STD_LOGIC;
		 Q : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end Counter;

--}} End of automatically maintained section

architecture Counter of Counter is	  
signal count : STD_logic_vector (3 downto 0) :=(others => '0');
begin

	process (CLK,RST) 
	begin	  
		if RST = '1' then
			count <= (others =>'0')	  ;
		elsif CLK'event and CLK = '1' then	
			count<=count+1;
			end if;
		end process;	  
		Q<=count;

end Counter;
