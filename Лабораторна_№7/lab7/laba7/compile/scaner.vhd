-------------------------------------------------------------------------------
--
-- Title       : No Title
-- Design      : laba7
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : c:\My_Designs\lab7\laba7\compile\scaner.vhd
-- Generated   : 04/24/17 18:00:08
-- From        : c:\My_Designs\lab7\laba7\src\scaner.asf
-- By          : FSM2VHDL ver. 5.0.7.2
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.std_logic_arith.all;
use IEEE.std_logic_unsigned.all;

entity scaner is 
	port (
		CLK: in STD_LOGIC;
		Ret: in STD_LOGIC_VECTOR (3 downto 0);
		KeyCode: out STD_LOGIC_VECTOR (7 downto 0);
		Strobe: out STD_LOGIC;
		Scan: inout STD_LOGIC_VECTOR (3 downto 0));
end scaner;

architecture scaner_arch of scaner is

-- diagram signals declarations
signal Cond: STD_LOGIC;

-- SYMBOLIC ENCODED state machine: Sreg0
type Sreg0_type is (
    S10, S6, S7, S8, S9, S11
);
-- attribute enum_encoding of Sreg0_type: type is ... -- enum_encoding attribute is not supported for symbolic encoding

signal Sreg0: Sreg0_type;

begin


----------------------------------------------------------------------
-- Machine: Sreg0
----------------------------------------------------------------------
Sreg0_machine: process (CLK)
begin
	if CLK'event and CLK = '1' then
		-- Set default values for outputs, signals and variables
		Cond <= Ret(0) or Ret(1) or  Ret(2) or  Ret(3) ;
		case Sreg0 is
			when S10 =>
				if Cond = '1' then	
					Sreg0 <= S9;
				elsif Cond = '0' then	
					Sreg0 <= S11;
				end if;
			when S6 =>
				if Cond = '1' then	
					Sreg0 <= S9;
				elsif Cond = '0' then	
					Sreg0 <= S10;
				end if;
			when S7 =>
				if Cond = '0' then	
					Sreg0 <= S6;
				elsif Cond = '1' then	
					Sreg0 <= S9;
				end if;
			when S8 =>
				if Cond = '1' then	
					Sreg0 <= S7;
				elsif Cond = '0' then	
					Sreg0 <= S8;
				end if;
			when S9 =>
				if Cond = '0' then	
					Sreg0 <= S8;
				elsif Cond = '1' then	
					Sreg0 <= S9;
				end if;
			when S11 =>
				if Cond = '0' then	
					Sreg0 <= S8;
				elsif Cond = '1' then	
					Sreg0 <= S9;
				end if;
--vhdl_cover_off
			when others =>
				null;
--vhdl_cover_on
		end case;
	end if;
end process;

-- signal assignment statements for combinatorial outputs
Scan_assignment:
Scan <= "0100" when (Sreg0 = S10) else
        "0010" when (Sreg0 = S6) else
        "0001" when (Sreg0 = S7) else
        "1111" when (Sreg0 = S8) else
        "1000" when (Sreg0 = S11) else
        "1111";

Strobe_assignment:
Strobe <= '0' when (Sreg0 = S8) else
          '1' when (Sreg0 = S9) else
          '0';

KeyCode_assignment:
KeyCode <= "00000000" when (Sreg0 = S8) else
           Ret&Scan when (Sreg0 = S9) else
           "00000000";

end scaner_arch;
