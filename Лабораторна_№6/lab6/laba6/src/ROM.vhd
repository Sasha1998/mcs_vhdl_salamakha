-------------------------------------------------------------------------------
--
-- Title       : ROM
-- Design      : laba6
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : ROM.vhd
-- Generated   : Mon Apr 24 17:45:47 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {ROM} architecture {ROM}}

library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use ieee.std_logic_unsigned.all;
use ieee.numeric_std.ALL;


entity ROM is
	 port(
		 CEO : in STD_LOGIC;
		 Addr : in STD_LOGIC_VECTOR(3 downto 0);
		 Dout : out STD_LOGIC_VECTOR(3 downto 0)
	     );
end ROM;

--}} End of automatically maintained section

architecture ROM of ROM is
type Memory is array (0 to 15) of 	std_logic_vector(3 downto 0);
constant ROM:Memory := ( "1111",
                         "1110",
                         "1101", 
                         "1100",						 
                         "1011",						 
                         "1010",						 
                         "1001",						 
                         "1000",					 
                         "0111",						 
                         "0110",						 
                         "0101",						 
                         "0100",						 
                         "0011",						 
                         "0010",						 
                         "0001",						 
                         "0000") ;
begin
	process	(CEO)  
	begin
	    if CEO = '1'  
		then   
		Dout <= ROM(CONV_INTEGER(Addr));
	end if;
	end process;
end ROM;

