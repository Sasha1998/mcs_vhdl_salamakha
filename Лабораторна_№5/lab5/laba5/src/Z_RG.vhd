-------------------------------------------------------------------------------
--
-- Title       : Z_RG
-- Design      : laba5
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : Z_RG.vhd
-- Generated   : Mon Apr 24 17:39:24 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Z_RG} architecture {Z_RG}}
library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Z_RG is
	 port(
		 DATA_in : in STD_LOGIC;
		 CLK : in STD_LOGIC;
		 WE : in STD_LOGIC;
		 RE : in STD_LOGIC;
		 DATA_out : out STD_LOGIC_VECTOR(7 downto 0)
	     );
end Z_RG;

--}} End of automatically maintained section

architecture Z_RG of Z_RG is
begin

		process
	variable DATA : std_logic_vector(7 downto 0);
	begin		
	wait until CLK = '1';
	if WE = '1' and RE = '0'
		then   for i in 0 to 6 loop
	             DATA(7 - i) := DATA(6 - i);
		       end loop;
	           DATA(0)  := DATA_in;
	   elsif  WE = '0' and RE = '1'  
		then  DATA_out <= DATA;
		else  DATA_out <= "ZZZZZZZZ";
	end if;
	end process;


end Z_RG;
