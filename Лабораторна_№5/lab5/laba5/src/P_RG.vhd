-------------------------------------------------------------------------------
--
-- Title       : P_RG
-- Design      : laba5
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : P_RG.vhd
-- Generated   : Mon Apr 24 17:37:36 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {P_RG} architecture {P_RG}}

library IEEE;
use IEEE.STD_LOGIC_1164.all; 
use ieee.std_logic_unsigned.all;

entity P_RG is
	 port(
		 CLK : in STD_LOGIC;
		 WE : in STD_LOGIC;
		 RE : in STD_LOGIC;
		 DATA_in : in STD_LOGIC_VECTOR(7 downto 0);
		 DATA_out : out STD_LOGIC_VECTOR(7 downto 0)
	     );
end P_RG;

--}} End of automatically maintained section

architecture P_RG of P_RG is
begin
		process
	variable DATA : std_logic_vector(7 downto 0);
	begin
	wait until CLK = '1';
	if WE = '1' and RE = '0'
		then DATA := DATA_in;
	elsif  WE = '0' and RE = '1'  
		then  DATA_out <= DATA;
		else  DATA_out <= "ZZZZZZZZ";
	end if;
	end process;
end P_RG;

