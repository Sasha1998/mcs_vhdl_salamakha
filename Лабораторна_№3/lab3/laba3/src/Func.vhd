-------------------------------------------------------------------------------
--
-- Title       : Func
-- Design      : laba3
-- Author      : ����
-- Company     : ����
--
-------------------------------------------------------------------------------
--
-- File        : Func.vhd
-- Generated   : Mon Apr 24 17:09:17 2017
-- From        : interface description file
-- By          : Itf2Vhdl ver. 1.22
--
-------------------------------------------------------------------------------
--
-- Description : 
--
-------------------------------------------------------------------------------

--{{ Section below this comment is automatically maintained
--   and may be overwritten
--{entity {Func} architecture {Func}}

library IEEE;
use IEEE.STD_LOGIC_1164.all;

entity Func is
	 port(
		 X : in STD_LOGIC_VECTOR(2 downto 0);
		 Y : out STD_LOGIC
	     );
end Func;

--}} End of automatically maintained section

architecture Func of Func is	

signal A : Std_logic;
signal B : std_logic;
signal C : std_logic;
signal D : std_logic;
signal E : std_logic;
signal F : std_logic;

begin
 
	A <=   NOT (X (0)) after 5 ns;
	B <=   A OR X(1)  after 10 ns;
	C <=   A NOR X(2) after 10 ns;
	D <=  B AND C after 10 ns;
	E <=  NOT (C XOR X(2)) after 10 ns;
	F <=  B XOR D after 10 ns;
	Y <=  NOT (F AND E) after 10 ns;
	end Func;

